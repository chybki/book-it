import { Component, OnInit } from '@angular/core';
import { ICalEvent } from './modules/calendar/interfaces/calendar.interface';

import { EventsService } from './services/events.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private eventsService: EventsService) {}
  title = 'bookIt';
  events: Array<ICalEvent>;

  public getEvents(monthKey: string) {
    this.eventsService.fetch('bookit-test', monthKey).subscribe(res => {
      this.events = res
    });
  }

  ngOnInit(): void {
  }
}
