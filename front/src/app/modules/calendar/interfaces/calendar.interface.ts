
export interface ICalendar {
  month: ICalendarMonth;
  weekDays: Array<string>;
  monthsList: Array<IMonthObject>;
  setedMonth: IMonthObject;
  setedDay?: ICalDay;
}

export interface ICalendarMonth {
  prevWeek: Array<ICalDay>;
  selectedMonth: Array<ICalDay>;
  nextWeek: Array<ICalDay>;
}

export interface ICalDay {
  date: Date;
  events?: Array<ICalEvent>;
  current: boolean;
}

export interface IMonthObject {
  id: number;
  name: string;
  current: boolean;
}

export interface ICalEvent {
  timeStart?: Date;
  timeEnd?: Date;
  status: 'occupied' | 'aprroved' | 'confirmed' | 'wrong' | 'open';
}

export interface ISlotLabels {
  active: string;
  disabled: string;
}