import { Injectable, OnInit } from '@angular/core';
import { ICalendar, ICalDay, ICalendarMonth, IMonthObject, ICalEvent } from '../interfaces/calendar.interface';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ManageCalendarService{
  
  // for defalut run of counstructor method and inner uses
  private today: Date = new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()));
  
  private eventsSlotsAmount: number = 12;

  private timelineStartHour: number = 9;

  private calendar = this.generateCalendar(this.today.getMonth(), this.today.getFullYear());

  private calendarSubject: BehaviorSubject<ICalendar> = new BehaviorSubject<ICalendar>(this.calendar);

  public calendar$: Observable<ICalendar> = this.calendarSubject.asObservable();

  constructor () {
  }

  // main method which create Calendar object with use of above methods 
  public generateCalendar(month: number, year: number): ICalendar {
    const generatedMonth = this.generateMonth(month, year),
      generatedWeekdays = this.generateWeekDays(),
      generatedMonthsList = this.generateMonthlist(month);
    return  {
      month: generatedMonth,
      weekDays: generatedWeekdays,
      monthsList: generatedMonthsList,
      setedMonth: generatedMonthsList[month],
      setedDay: generatedMonth.selectedMonth.find(day => day.current)
    }
  }

  //allows to change only month object in calendar object by passed value
  public setMonth(month: number): void {
    if(month >= this.today.getMonth()) {
      this.calendar.month = this.generateMonth(month, this.today.getFullYear());
      this.calendar.setedMonth = this.calendar.monthsList[month];
    }
  }

  //allows to set day object in calendar object by passed value
  public setDay(day: ICalDay): void {
    if(this.today <= day.date)
      for(let calEl in this.calendar.month) {
        this.findDayToSet(calEl, day);
      }
  }

  public userSetup(slots: number, events: Array<ICalEvent>) {
    //this.eventsSlotsAmount = slots;
    if(events != undefined)
      this.setEvents(events);
  }

  private setEvents(events: Array<ICalEvent>) {
    console.log(events)
    for(let calElIndex in this.calendar.month) {
      for(let eventIndex in events ) {        
        let datIndex = this.calendar.month[calElIndex].findIndex(day => day.date.toJSON().split('T')[0] == new Date(events[eventIndex].timeStart).toJSON().split('T')[0]);
        if(datIndex != -1) {
          let slotIndex = this.calendar.month[calElIndex][datIndex].events.findIndex(event => event.timeStart.toJSON() == new Date(events[eventIndex].timeStart).toJSON());
            events[eventIndex].timeStart = new Date(events[eventIndex].timeStart);
            events[eventIndex].timeEnd = new Date(events[eventIndex].timeEnd);
          this.calendar.month[calElIndex][datIndex].events[slotIndex] = events[eventIndex];
        }
      }
    }
  }

  //day finder
  private findDayToSet(calEl: string, day: ICalDay): void {    
    this.calendar.month[calEl].find((dayEl: ICalDay) => {
      if(dayEl === day) {
        this.calendar.setedDay = dayEl;
        this.navToOtherMonth(day);
      }
    })
  }

  //
  private navToOtherMonth(day: ICalDay) {
    if(day.date.getMonth() !== this.calendar.setedMonth.id) {
      this.setMonth(day.date.getMonth());
    }
  }

  private generateMonth(month: number, year: number): ICalendarMonth {
    const CURRMONTH = [];

    CURRMONTH.push(this.getDaysOfMonth(month, year));    
    CURRMONTH.unshift(this.prevWeek(CURRMONTH[0]));
    CURRMONTH.push(this.nextWeek(CURRMONTH[1],CURRMONTH[0].length));
    return {
      prevWeek: CURRMONTH[0],
      selectedMonth: CURRMONTH[1],
      nextWeek: CURRMONTH[2]
    };
  }

  private getDaysOfMonth(month: number, year: number): Array<ICalDay> {
    const DATE = new Date(Date.UTC(year, month, 1));
    const DAYS = [];

    while (DATE.getMonth() === month) {
        DAYS.push({
          date: new Date(DATE),
          current: this.isCurr(DATE),
          events: this.generateEventsSlots(DATE)
        });
        DATE.setDate(new Date(DATE).getDate() + 1);
    }
    return DAYS;
  }

  private isCurr(date: Date): boolean {
    return date.getDate() === this.today.getDate()
    && date.getMonth() === this.today.getMonth()
    && date.getFullYear() === this.today.getFullYear()
  }

  private nextWeek(month: Array<ICalDay>, prevWeekL: number): Array<ICalDay> {
    const DATE = new Date(Date.UTC(month[0].date.getFullYear(), (month[0].date.getMonth()) + 1, 1));
    DATE.setDate(new Date(DATE).getDate() + (DATE.getDay() !== 0 ? (7 - DATE.getDay()) : 0) + this.supposeAddWeek(prevWeekL + month.length));
    const WEEK = [];
    while (DATE.getDate() < month[month.length - 1].date.getDate()) {
      WEEK.push({
        date: new Date(DATE),
        current: this.isCurr(DATE),
        events: this.generateEventsSlots(DATE)
      });
      DATE.setDate(new Date(DATE).getDate() - 1);
    }
    WEEK.reverse();
    return WEEK;
  }

  private supposeAddWeek(days: number): number {
    if(days < 35) {
      return 7;
    } else {
      return 0;
    }
  }

  private prevWeek(month: Array<ICalDay> ): Array<ICalDay> {
    const DATE = new Date(Date.UTC(month[0].date.getFullYear(), (month[0].date.getMonth()), 0));
    DATE.setDate(new Date(DATE).getDate() - (DATE.getDay() !== 0 ? (DATE.getDay() - 1) : 6) );
    const WEEEK = [];
    while (DATE.getDate() > month[0].date.getDate() ) {
      WEEEK.push({
        date: new Date(DATE),
        current: this.isCurr(DATE),
        events: this.generateEventsSlots(DATE)
      });
      DATE.setDate(new Date(DATE).getDate() + 1);
    }
    return WEEEK;
  }

  private generateMonthlist(month?: number): Array<IMonthObject> {
    const EN = new Intl.DateTimeFormat('en', { month: 'long' });
    const MONTHS = [];
    let date;
    let tmpMonth;

    for (let i = 0; i < 12; i++) {
      date = new Date(2020, i);
      tmpMonth = EN.format(new Date(date));
      MONTHS.push({name: tmpMonth, current: (i === this.today.getMonth() ? true : false), id: i});
    }
    return MONTHS;
  }

  private generateWeekDays(): Array<string> {
    const EN = new Intl.DateTimeFormat('en', { weekday: 'short' });
    const WEEEKDAYS = [];
    let date;
    let tmpDate;

    for (let i = 0; i < 7; i++) {
      date = new Date(Date.UTC(2017, 0, (2 + i)));
      tmpDate = EN.format(new Date(date));
      WEEEKDAYS.push(tmpDate);
    }
    return WEEEKDAYS;
  }

  private generateEventsSlots(date: Date): Array<ICalEvent> {
    const eventsSlots: Array<ICalEvent> = [];
    for(let i = 0; i < this.eventsSlotsAmount; i++) {
      eventsSlots.push({
        timeStart: new Date(date.setUTCHours(this.timelineStartHour + i ,0, 0)),
        timeEnd: new Date(date.setUTCHours(((this.timelineStartHour + i) + 1),0, 0)),
        status: 'open',
      })
    }

    return eventsSlots
  }
}
