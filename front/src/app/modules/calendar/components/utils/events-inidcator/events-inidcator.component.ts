import { Component, OnInit, Input } from '@angular/core';
import { ICalEvent } from '../../../interfaces/calendar.interface';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-events-inidcator',
  templateUrl: './events-inidcator.component.html',
  styleUrls: ['./events-inidcator.component.scss']
})
export class EventsDayInidcatorComponent implements OnInit {

  @Input() events;
  @Input() selected: boolean;

  constructor() { }

  public open: Array<ICalEvent> = [];

  private createOpenAmount(events: Array<ICalEvent>): number {
    return events.filter(event => event.status === 'open').length;
  }

  ngOnInit() {
  }

}
