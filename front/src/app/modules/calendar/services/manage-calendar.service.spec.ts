import { TestBed } from '@angular/core/testing';

import { ManageCalendarService } from './manage-calendar.service';

describe('ManageCalendarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ManageCalendarService = TestBed.get(ManageCalendarService);
    expect(service).toBeTruthy();
  });
});
