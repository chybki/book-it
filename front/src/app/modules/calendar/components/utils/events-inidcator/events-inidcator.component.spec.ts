import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsDayInidcatorComponent } from './events-inidcator.component';

describe('EventsDayInidcatorComponent', () => {
  let component: EventsDayInidcatorComponent;
  let fixture: ComponentFixture<EventsDayInidcatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsDayInidcatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsDayInidcatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
