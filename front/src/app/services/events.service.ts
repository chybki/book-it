import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class EventsService {
  
  constructor(private http: HttpClient) { }

  private host: string = "http://localhost:999/api/"
  fetch(calendar, month): Observable<any> {
    return this.http.get(`${this.host}events`, { params: {calendar, month} });
  }
}
