import { Subscription } from 'rxjs';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ManageCalendarService } from '../../services/manage-calendar.service';
import { ICalDay, ICalendar, ICalEvent, ISlotLabels } from '../../interfaces/calendar.interface';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit, OnDestroy {

  constructor ( private calendarService: ManageCalendarService ) {}

  public subscription: Subscription;
  public calendar: ICalendar;
  public labels: ISlotLabels = {
    active: 'BookIt now',
    disabled: 'BookIted'
  }

  
  public today: Date = new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(),1,0,0,0));

  public isAfterCurrent(date: Date): boolean {
    return new Date(date.setUTCHours(1,0,0,0)) > this.today
  }

  public nextDay(day: ICalDay) {
    let dayIndex = this.calendar.month.selectedMonth.indexOf(day);
    this.calendar.setedDay = this.calendar.month.selectedMonth[dayIndex + 1]
  }

  public prevDay(day: ICalDay) {
    let dayIndex = this.calendar.month.selectedMonth.indexOf(day);
    this.calendar.setedDay = this.calendar.month.selectedMonth[dayIndex - 1]
  }

  ngOnInit() {
    this.subscription = this.calendarService.calendar$.subscribe(calendar => this.calendar = calendar);
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }
}
