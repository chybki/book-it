import { Component, OnInit, Input } from '@angular/core';
import { ISlotLabels, ICalEvent } from '../../../interfaces/calendar.interface';

@Component({
  selector: 'app-event-slot',
  templateUrl: './event-slot.component.html',
  styleUrls: ['./event-slot.component.scss']
})
export class EventSlotComponent implements OnInit {

  @Input() timeStart: Date;
  @Input() timeEnd?: Date;
  @Input() status: string;
  @Input() slotLabels: ISlotLabels;

  constructor() { 
  }

  public hour: string;
  public slotLabel: string;
  
  public getHour(date: Date): string {
    return date != undefined ? date.toJSON().split('T')[1].substring(0, 5) : '';
  }

  public isActive(status: string): boolean {
    if(status === 'open') {
      return true;
    }
    return false
  }

  ngOnInit() {
    this.hour = (!this.timeEnd || this.timeEnd != undefined ? this.getHour(this.timeStart) : this.getHour(this.timeStart) + ' - ' + this.getHour(this.timeEnd));
    this.slotLabel = this.isActive(this.status) ? this.slotLabels.active : this.slotLabels.disabled;
  }

}
