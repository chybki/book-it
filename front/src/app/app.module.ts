import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarComponent, TimelineComponent, EventSlotComponent, EventsDayInidcatorComponent} from './modules/calendar/';
import { NavComponent } from './components/nav/nav.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SideHeaderComponent } from './components/utils/side-header/side-header.component';
import { EventsService } from './services/events.service';
import { HttpClientModule } from '@angular/common/http';
import { DayComponent } from './modules/calendar/components/utils/day/day.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    NavComponent,
    SidebarComponent,
    SideHeaderComponent,
    TimelineComponent,
    EventSlotComponent,
    EventsDayInidcatorComponent,
    DayComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [EventsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
