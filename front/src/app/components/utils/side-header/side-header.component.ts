import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-side-header',
  templateUrl: './side-header.component.html',
  styleUrls: ['./side-header.component.scss']
})
export class SideHeaderComponent implements OnInit {

  @Input() text: string;

  constructor() { }

  ngOnInit() {
  }

}
