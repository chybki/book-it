import { ICalDay } from './../../../interfaces/calendar.interface';
import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss']
})
export class DayComponent implements OnInit {

  @Input() day: ICalDay;
  @Input() selected: boolean;

  constructor() { }
  
  public today: Date = new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()));
  public dayVM: ICalDay;
  public isAfterCurrent(date: Date): boolean {
    return date >= this.today
  }

  ngOnInit(): void {
  }

}
