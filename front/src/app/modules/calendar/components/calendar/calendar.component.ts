import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ICalendar, ICalDay, ICalEvent, ICalendarMonth } from '../../interfaces/calendar.interface';
import { ManageCalendarService } from '../../services/manage-calendar.service';
import { Subscription, BehaviorSubject, Observable } from 'rxjs';
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, OnDestroy {

  private _events = new BehaviorSubject<ICalEvent[]>([]);

  @Input() 
    set events(value) { this._events.next(value)};
    get events() { return this._events.getValue() }
  @Input() slots: number;

  @Output() monthKey = new EventEmitter<Array<string>>();

  constructor(private calendarService: ManageCalendarService) {
  }

  public subscription: Subscription;
  public calendar: ICalendar;
  public today: Date = new Date(Date.UTC(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()));

  public getMonth(month: number): void {
    this.monthKey.emit([this.monthKeyParser(month - 1), this.monthKeyParser(month), this.monthKeyParser(month + 1)])
    this.calendarService.setMonth(month);
  }

  public selectDay(day: ICalDay): void {
    if(day.date.getMonth() != this.calendar.setedMonth.id) {
      this.monthKey.emit([this.monthKeyParser(day.date.getMonth() - 1), this.monthKeyParser(day.date.getMonth()), this.monthKeyParser(day.date.getMonth() + 1)])
    }

    this.calendarService.setDay(day);
  }

  public isSetDay(day: ICalDay): boolean {
    return this.calendar.setedDay ? day.date.toDateString() === this.calendar.setedDay.date.toDateString() : false;
  }

  public isAfterCurrent(date: Date): boolean {
    return date >= this.today
  }

  public setedDateDisplayer (): string {
    if(this.calendar.setedDay) {
      return this.formatToDateString(this.calendar.setedDay.date);
    }
    return this.calendar.setedMonth.name;
  }

  private formatToDateString(date: Date): string {
    const tmpDate = date.toJSON().split('T')[0].split('-');
    
    return tmpDate[2] + '.' + tmpDate[1] + '.' + tmpDate[0]
  }

  private monthKeyParser(monthId: number) {
    return monthId < 10 ? `0${monthId + 1}${this.today.toJSON().split('T')[0].split('-')[0].slice(2,4)}` : `${monthId + 1}${this.today.toJSON().split('T')[0].split('-')[0].slice(2,4)}`;
  }

  ngOnInit() {
    this.monthKey.emit([this.monthKeyParser(this.today.getMonth() - 1), this.monthKeyParser(this.today.getMonth()), this.monthKeyParser(this.today.getMonth() + 1)]);

    this._events.subscribe(x => {
      this.calendarService.userSetup(this.slots, this.events);
    });
    
    this.subscription = this.calendarService.calendar$.subscribe(calendar => {
      console.log(calendar);
      this.calendar = calendar;
    })
  }

  ngOnDestroy () {
    this.subscription && this.subscription.unsubscribe();
  }
}
